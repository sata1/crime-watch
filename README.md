# CrimeWatch

## Dev environment

Developed with Elixir 1.7.0 and Erlang/OTP 19.3. the Mix binary in the
repo is built with it.

Nvm was used to manage node version, developed with V6.14.1.

GNU/Linux Debian Buster/sid as OS.

## Starting
setup environment with

```
$ make ZIP=/path/to/zip_archive setup
```

Once setup is done start Phoenix server with

```
$ make dev
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
