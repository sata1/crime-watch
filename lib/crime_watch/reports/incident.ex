defmodule CrimeWatch.Reports.Incident do
  use Ecto.Schema
  import Ecto.Changeset

  require Ecto.Query

  @key_order_asc "0"
  @key_order_desc "1"

  schema "incidents" do
    field :date, :string
    field :crime_type, :string
    field :crime_id, :string,     default: ""
    field :reported_by, :string,  default: ""
    field :falls_within, :string, default: ""
    field :longitude, :float,     default: 0.0
    field :latitude, :float,      default: 0.0
    field :location, :string,     default: ""
    field :lsoa_code, :string,    default: ""
    field :lsoa_name, :string,    default: ""
    field :last_outcome, :string, default: ""
    field :context, :string,      default: ""

    timestamps()
  end

  def changeset(incident, attrs) do
    incident
    |> cast(attrs, [:crime_id, :date, :reported_by, :falls_within, :longitude, :latitude, :location, :lsoa_code, :lsoa_name, :crime_type, :last_outcome, :context])
    |> validate_required([:date, :crime_type])
    |> validate_format(:date, ~r/^\d{4}-\d{2}$/)
  end

  def search(query, nil) do
    query
  end
  def search(query, "") do
    query
  end
  def search(query, term) do
    wildcard_search = "%#{term}%"

    Ecto.Query.from incident in query,
      where: ilike(incident.crime_type, ^wildcard_search)
  end

  def get_order(@key_order_asc), do: :asc
  def get_order(@key_order_desc), do: :desc
  def get_order(_),   do: :asc

  def invert_order(@key_order_asc), do: @key_order_desc
  def invert_order(@key_order_desc), do: @key_order_asc
  def invert_order(_), do: @key_order_desc

  def asc_order(), do: @key_order_asc
  def desc_order(), do: @key_order_desc

  def sort(query, order), do: do_sort(query, get_order(order))

  defp do_sort(query, order) do
    query |> Ecto.Query.order_by([{^order, :date}])
  end
end
