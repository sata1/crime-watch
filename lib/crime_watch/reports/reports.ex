defmodule CrimeWatch.Reports do
  @moduledoc """
  The Reports context.
  """

  alias CrimeWatch.Repo

  alias CrimeWatch.Reports.Incident

  @doc """
  Returns the list of incidents with pagination, sorting and searching
  """
  def paginate_list_incidents(params, order, search_terms) do
    Incident
    |> Incident.search(search_terms)
    |> Incident.sort(order)
    |> Repo.paginate(params)
  end

  @doc """
  Gets a single incident.

  Raises `Ecto.NoResultsError` if the Incident does not exist.
  """
  def get_incident!(id) when is_integer(id) do
    Repo.get!(Incident, id)
  end

  @doc """
  Gets a single incident.

  Returns nil if the Incident does not exist.
  """
  def get_incident(id) do
    Repo.get(Incident, id)
  end

  @doc """
  Creates a incident.

  ## Examples

      iex> create_incident(%{field: value})
      {:ok, %Incident{}}

      iex> create_incident(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_incident(map()) :: {:ok, Incident} | {:error, Ecto.Changeset}
  def create_incident(attrs \\ %{}) do
    %Incident{}
    |> Incident.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking incident changes.

  ## Examples

      iex> change_incident(incident)
      %Ecto.Changeset{source: %Incident{}}

  """
  def change_incident(%Incident{} = incident) do
    Incident.changeset(incident, %{})
  end


  @doc """
  Creates incidents from a batch of parsed CSV lines, can be used together
  with Parser.parse_stream |> Stream.chunk_every(100).

  """
  def create_from_batch(data) when is_list(data) do
    fields = Incident.__schema__(:fields) -- [:id]
    extra_fields = %{inserted_at: Ecto.DateTime.utc,
                     updated_at: Ecto.DateTime.utc}

    incidents = Enum.map(data,
      fn (incident) ->
        Map.take(incident,fields)
        |> Map.merge(extra_fields)
      end)

    Repo.insert_all(Incident, incidents)
  end
end
