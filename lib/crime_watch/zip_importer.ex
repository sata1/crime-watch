defmodule CrimeWatch.ZipImporter do
  alias CrimeWatch.Reports.Incident

  require Record
  require Logger

  Record.defrecord :zip_file, Record.extract(:zip_file,
    from_lib: "stdlib/include/zip.hrl")

  NimbleCSV.define(Parser, separator: ",", escape: "\"")

  @doc """
  Imports CSV entries from archive. Assumes CSV structure follows data.police.uk archives
  'archive' is a file path to zip archive.
  """
  @spec import_archive(String.t) :: :ok | {:error, any}
  def import_archive(archive) do
    {:ok, tmp_dir} = Temp.path()
    :zip.zip_open(:binary.bin_to_list(archive), [{:cwd, tmp_dir}])
    |> case do
         {:error, reason} ->
           {:error, {:cannot_open, reason}}
         {:ok, handler} ->
           {:ok, archive_list} = :zip.zip_list_dir(handler)
           process_archive(archive_list, handler)
           :ok = :zip.zip_close(handler)
           File.rm_rf!(tmp_dir)
           :ok
       end
  end

  defp process_archive(list, handler) do
    list
    |> get_filenames()
    |> Enum.map(
    fn(filename) ->
      {:ok, unzipped_file} = :zip.zip_get(filename, handler)
      process_file(unzipped_file)
    end)
  end

  defp get_filenames(list) do
    List.foldl(list, [],
      fn
        (e, acc) when Record.is_record(e, :zip_file) ->
          [zip_file(e, :name) | acc]
        (_, acc) ->
          acc
      end)
      |> Enum.sort()
  end

  defp process_file(path) do
    stream = File.stream!(path, read_ahead: 64 * 1024)
    |> Parser.parse_stream
    |> Task.async_stream(&process_line/1)

    Stream.run(stream)
  end

  defp process_line([crime_id, date, reported_by, falls_within,
                     longitude, latitude, location, lsoa_code,
                     lsoa_name, crime_type, outcome, context] = line) do

    attr = %{
      crime_id: crime_id,
      date: date,
      reported_by: reported_by,
      falls_within: falls_within,
      longitude: longitude,
      latitude: latitude,
      location: location,
      lsoa_code: lsoa_code,
      lsoa_name: lsoa_name,
      crime_type: crime_type,
      last_outcome: outcome,
      context: context
    }

    case CrimeWatch.Reports.create_incident(attr) do
      {:error, _changeset} ->
        Logger.warn("incident validation failed for: #{line}")
      {:ok, %Incident{}} ->
        :ok
    end
  end

  defp process_line(list) do
    Logger.warn("Unexpected CSV line #{list}")
  end
end
