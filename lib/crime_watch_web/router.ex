defmodule CrimeWatchWeb.Router do
  use CrimeWatchWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CrimeWatchWeb do
    pipe_through :browser # Use the default browser stack

    get "/", IncidentController, :index

    resources "/incidents", IncidentController, only: [:index, :show]
  end

  # Other scopes may use custom stacks.
  # scope "/api", CrimeWatchWeb do
  #   pipe_through :api
  # end
end
