defmodule CrimeWatchWeb.IncidentController do
  use CrimeWatchWeb, :controller

  alias CrimeWatch.Reports

  @sort :sort
  @sort_str Atom.to_string(@sort)
  @query :query
  @query_str Atom.to_string(@query)

  def index(conn, params) do
    order = Map.get(params, @sort_str, nil)
    search_terms = Map.get(params, @query_str, nil)

    page = Reports.paginate_list_incidents(params, order, search_terms)

    extra_fields = [{@sort, order}, {@query, search_terms}]
    render(conn, "index.html", incidents: page.entries, page: page, extra: extra_fields)
  end

  def show(conn, %{"id" => id}) do
    case Reports.get_incident(id) do
      nil ->
        conn
        |> put_status(:not_found)
        |> text("Incident not found")

      incident ->
        render(conn, "show.html", incident: incident)
    end
  end
end
