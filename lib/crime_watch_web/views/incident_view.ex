defmodule CrimeWatchWeb.IncidentView do
  use CrimeWatchWeb, :view
  import Scrivener.HTML
  import CrimeWatch.Reports.Incident, only: [get_order: 1, invert_order: 1]

  alias CrimeWatchWeb.Router.Helpers
  alias Phoenix.HTML.Link

  def sort_date_link(conn, extra) do
    path = Helpers.incident_path(conn, :index, swap_order(extra))
    Link.link("Date" <> arrow(extra), to: path)
  end

  def swap_order(extra) do
    Keyword.update!(extra, :sort, fn curr -> invert_order(curr) end)
  end

  def arrow(fields) do
    case get_order(Keyword.get(fields, :sort)) do
      :asc  -> "↑"
      :desc -> "↓"
    end
  end
end
