defmodule Ingest do
  @moduledoc """

  Populates crime_watch app with provided zip archive from data.police.uk

  Example:

  $ mix run priv/repo/seeds.exs --archive path/to/archive.zip

  """
  require Logger

  alias CrimeWatch.ZipImporter

  def main(args) do
    case OptionParser.parse(args, switches: [archive: String]) do
      {[archive: path], _rest, _invalid} ->
        start_time = DateTime.utc_now()
        Logger.info("Started to import")
        case ZipImporter.import_archive(path) do
          :ok ->
            secs = Time.diff(DateTime.utc_now(), start_time, :seconds)
            Logger.info("Archive processed in: #{secs} seconds")
          {:error, {:cannot_open, reason}} ->
            Logger.error("Couldn't open zip archive with reason #{reason}.")
        end
      _ ->
        handle_missing_archive()
    end
  end

  defp handle_missing_archive() do
    script = __ENV__.file
    Logger.error("""
    Missing option --archive for script

    Example: mix run #{script} --archive path/to/archive.zip

    """)
  end

end

Ingest.main(System.argv)
