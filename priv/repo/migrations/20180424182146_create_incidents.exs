defmodule CrimeWatch.Repo.Migrations.CreateIncidents do
  use Ecto.Migration

  def change do
    create table(:incidents) do
      add :crime_id, :string
      add :date, :string
      add :reported_by, :string
      add :falls_within, :string
      add :longitude, :float
      add :latitude, :float
      add :location, :string
      add :lsoa_code, :string
      add :lsoa_name, :string
      add :crime_type, :string
      add :last_outcome, :string
      add :context, :string

      timestamps()
    end

    create index(:incidents, [:date])
  end
end
