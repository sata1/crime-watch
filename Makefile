.PHONY: deep-clean clean clean_db deps deps-tree test xref dev routes dialyzer unit todo compile setup assets

MIX = ./mix

.DEFAULT_GOAL := dev

deep-clean:
	$(MIX) clean --deps

clean:
	$(MIX) clean

clean_db:
	$(MIX) do ecto.drop, ecto.create, ecto.migrate
deps:
	$(MIX) deps.get

deps-tree:
	$(MIX) deps.tree

xref:
	$(MIX) do xref unreachable, xref deprecated

dialyzer:
	$(MIX) dialyzer

test: dialyzer xref unit

unit:
	$(MIX) test

dev:
	$(MIX) phx.server

routes:
	$(MIX) phx.routes

todo:
	grep -ri todo lib/ priv/ test/

compile:
	$(MIX) do deps.get, deps.compile, compile

assets:
	cd assets && npm install && cd ..

setup: deps assets compile clean_db
	$(MIX) run priv/repo/seeds.exs --archive ${ZIP}
