defmodule CrimeWatchWeb.IncidentControllerTest do
  use CrimeWatchWeb.ConnCase

  alias CrimeWatch.Reports

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Listing Incidents"
  end

  @create_attrs %{crime_id: "some crime id",
                  date: "2018-02",
                  reported_by: "MET police",
                  falls_within: "MET police",
                  longitude: 54.0,
                  latitude: 54.0,
                  location: "some location",
                  lsoa_code: "some LSOA code",
                  lsoa_name: "some LSOA name",
                  crime_type: "some crime type",
                  last_outcome: "some result",
                  context: "some context"}

  def fixture(:incident) do
    {:ok, incident} = Reports.create_incident(@create_attrs)
    incident
  end

  defp create_incident(_) do
    incident = fixture(:incident)
    {:ok, incident: incident}
  end

  describe "index" do
    test "lists all incidents", %{conn: conn} do
      conn = get conn, incident_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Incidents"
    end
  end

  describe "Not supporting create/edit/delete of incidents" do
    setup [:create_incident]

    defp expected_msg do
      expected_msg(2)
    end
    defp expected_msg(arity) do
      "No function clause for CrimeWatchWeb.Router.Helpers.incident_path/#{arity} and action :"
    end

    test "Check to see if exception is thrown when requesting form for new", %{conn: conn} do
      err = assert_raise(ArgumentError, fn -> get(conn, incident_path(conn, :new)) end)
      assert String.contains?(err.message, expected_msg() <> "new") == true
    end

    test "Check to see if exception is thrown when requesting form for edit", %{conn: conn} do
      err = assert_raise(ArgumentError, fn -> get(conn, incident_path(conn, :edit)) end)
      assert String.contains?(err.message, expected_msg() <> "edit") == true
    end

    test "Check to see if exception is thrown when requesting form for delete", %{conn: conn, incident: incident} do
      err = assert_raise(ArgumentError, fn -> get(conn, incident_path(conn, :delete, incident)) end)
      assert String.contains?(err.message, expected_msg(3) <> "delete") == true
    end

  end

  describe "show" do
    setup [:create_incident]

    test "show incident", %{conn: conn, incident: incident} do
      conn = get conn, incident_path(conn, :show, incident)
      assert html_response(conn, 200) =~ "some context"
    end

    test "Show 404 when incident not found", %{conn:  conn} do
      conn = get(conn, incident_path(conn, :show, -1))
      assert text_response(conn, 404) =~ "Incident not found"
    end

  end

end
