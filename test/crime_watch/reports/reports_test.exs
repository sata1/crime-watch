defmodule CrimeWatch.ReportsTest do
  use CrimeWatch.DataCase

  alias CrimeWatch.Reports

  describe "incidents" do
    alias CrimeWatch.Reports.Incident

    @valid_attrs %{crime_id: "some crime id",
                   date: "2018-02",
                   reported_by: "MET police",
                   falls_within: "MET police",
                   longitude: 54.0,
                   latitude: 54.0,
                   location: "some location",
                   lsoa_code: "some LSOA code",
                   lsoa_name: "some LSOA name",
                   crime_type: "some crime type",
                   last_outcome: "some outcome",
                   context: "some context"}

    @invalid_attrs %{crime_id: 123,
                   date: nil,
                   reported_by: nil,
                   falls_within: nil,
                   longitude: "incorrect",
                   latitude: "incorrect",
                   location: nil,
                   lsoa_code: nil,
                   lsoa_name: nil,
                   crime_type: nil,
                   last_outcome: nil,
                   context: 123}

    def fixture(attrs \\ %{}) do
      {:ok, incident} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Reports.create_incident()

      incident
    end

    def changeset(override \\ %{}), do: Incident.changeset(%Incident{}, Enum.into(override, @valid_attrs))

    def setup_order_fixtures() do
      for n <- 1..3 do
        n_str = Integer.to_string(n)

        fixture(%{date: "2018-" <> String.pad_leading(n_str, 2, "0"),
                  crime_id: n_str})
      end
    end

    test "paginate_list_incidents returns all incidents without any sorting or searching" do
      incident = fixture()
      assert Reports.paginate_list_incidents(nil, nil, nil).entries() == [incident]
    end

    test "paginate_list_incidents returns incidents matching a search term" do
      for _n <- 1..5, do: fixture(%{crime_type: "some robbery"})

      incident = fixture(%{crime_type: "some assault"})

      assert Reports.paginate_list_incidents(nil, nil, "assault").entries() == [incident]
    end

    test "paginate_list_incidents returns incidents in ascending order" do
      setup_order_fixtures()

      head = Reports.paginate_list_incidents(nil, Incident.asc_order(), nil).entries()
      |> hd

      assert Map.fetch!(head, :crime_id) == "1"
    end

    test "paginate_list_incidents returns incidents in descending order" do
      setup_order_fixtures()

      head = Reports.paginate_list_incidents(nil, Incident.desc_order(), nil).entries()
      |> hd

      assert Map.fetch!(head, :crime_id) == "3"
    end

    test "paginate_list_incidents default order is ascending" do
      setup_order_fixtures()

      head = Reports.paginate_list_incidents(nil, nil, nil).entries()
      |> hd

      assert Map.fetch!(head, :crime_id) == "1"
    end

    test "get_incident! returns the incident with given id" do
      incident = fixture()
      assert Reports.get_incident!(incident.id) == incident
    end

    test "Pass Incident changeset validation with correct data" do
      i = changeset()
      assert i.valid?
    end

    test "Fail Incident changeset validation with incorrect data" do
      cs = changeset(@invalid_attrs)
      assert cs.valid? == false
    end

    test "Fail incident changeset crime_type validation" do
      cs = changeset(%{crime_type: ""})
      assert %{crime_type: ["can't be blank"]} = errors_on(cs)
    end

    test "Fail when wrong date separator" do
      cs = changeset(%{date: "2018/02"})
      assert %{date: ["has invalid format"]} = errors_on(cs)
    end

    test "Fail when full date is passed" do
      cs = changeset(%{date: "2018-02-02"})
      assert %{date: ["has invalid format"]} = errors_on(cs)
    end

    test "Fail on empty date" do
      cs = changeset(%{date: ""})
      assert %{date: ["can't be blank"]} = errors_on(cs)
    end

    test "fail on nil date" do
      cs = changeset(%{date: nil})
      assert %{date: ["can't be blank"]} = errors_on(cs)
    end
  end
end
