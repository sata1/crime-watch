defmodule CrimeWatch.ZipImporterTest do
  use ExUnit.Case, async: true
  alias CrimeWatch.ZipImporter

  test "Expecting error return when trying to import non existant archive" do
    assert {:error, {:cannot_open, _reason}} = ZipImporter.import_archive("foo")
  end

end
