# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :crime_watch,
  ecto_repos: [CrimeWatch.Repo]

# Configures the endpoint
config :crime_watch, CrimeWatchWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "7aGObxeyvGcb1T9DdcVWcdWxNZ+/3CfucHiEOhMQ02oF+FQ1Ud34N1U6jCyt6dyv",
  render_errors: [view: CrimeWatchWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CrimeWatch.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :scrivener_html,
  routes_helper: CrimeWatch.Router.Helpers
